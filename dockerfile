From java:8

RUN apt-get update
RUN apt-get install -y maven

ADD . /data/app/
WORKDIR /data/app

RUN ["mvn","package"]

CMD ["java","-jar", "target/demo-boot-1.0-RELEASE.war"]
